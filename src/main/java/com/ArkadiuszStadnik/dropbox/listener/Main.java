package com.ArkadiuszStadnik.dropbox.listener;

import com.ArkadiuszStadnik.dropbox.listener.config.ConfigService;
import com.ArkadiuszStadnik.dropbox.mail.Email;
import com.ArkadiuszStadnik.dropbox.mail.EmailClient;
import com.ArkadiuszStadnik.dropbox.mail.EmailService;
import com.ArkadiuszStadnik.dropbox.mail.MailProvider;
import com.ArkadiuszStadnik.dropbox.upload.DropBoxUploader;

import java.io.IOException;

public class Main {
    private static final int PROPS_INDEX = 0;

    public static void main(String[] args) throws IOException, InterruptedException {
        String propsPath = args[PROPS_INDEX];
        ConfigService cfg = new ConfigService(propsPath).load();
//        EmailClient emailClient = MailProvider.get(cfg);
        MailProvider mailProvider = new MailProvider();
        EmailClient emailClient = mailProvider.get(cfg);
        EmailService emailService = new EmailService(emailClient);
        Email mail = emailService.createMail();
        DropBoxUploader dropBoxUploader = new DropBoxUploader(cfg,emailClient,mail);
        DirectoryListener directoryListener = new DirectoryListener(dropBoxUploader,cfg);
        directoryListener.listen();







//        MailProvider mailProvider = new MailProvider();
//        EmailService emailService = new EmailService();
//        DropBoxUploader dropBoxUploader = new DropBoxUploader(cfg, emailClient);
//        DirectoryListener directoryListener = new DirectoryListener(dropBoxUploader,cfg);
//        directoryListener.listen();

    }
}
