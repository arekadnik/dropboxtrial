package com.ArkadiuszStadnik.dropbox.listener;

import com.ArkadiuszStadnik.dropbox.listener.config.ConfigService;
import com.ArkadiuszStadnik.dropbox.upload.Uploader;

import java.io.IOException;
import java.nio.file.*;

import static com.ArkadiuszStadnik.dropbox.listener.config.Keys.DIRECTORY;

public class DirectoryListener {
    Uploader uploader;
    String dir;

    public DirectoryListener(Uploader uploader, ConfigService cfg) {
        this.uploader = uploader;
        this.dir = cfg.get(DIRECTORY);
    }

    public void listen() throws IOException, InterruptedException {

        WatchService watchService = FileSystems.getDefault().newWatchService();
        Path path = Paths.get(dir);
        path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
        WatchKey key;
        while ((key = watchService.take()) != null) {
            //TODO check if the is only single event
            String name = key.pollEvents().get(0).context().toString();
            System.out.println(name);
            uploader.upload(dir + name, name);

            key.reset();
        }
    }
}
