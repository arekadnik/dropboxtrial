package com.ArkadiuszStadnik.dropbox.upload;

import com.dropbox.core.DbxException;

public interface Uploader {

    void upload(String path, String name);
}
