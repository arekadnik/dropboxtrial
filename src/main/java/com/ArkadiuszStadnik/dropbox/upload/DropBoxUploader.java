package com.ArkadiuszStadnik.dropbox.upload;

import com.ArkadiuszStadnik.dropbox.listener.config.ConfigService;
import com.ArkadiuszStadnik.dropbox.mail.Email;
import com.ArkadiuszStadnik.dropbox.mail.EmailClient;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.users.FullAccount;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.ArkadiuszStadnik.dropbox.listener.config.Keys.DROPBOX_TOKEN;

public class DropBoxUploader implements Uploader {

    private final ConfigService cfg;
    private final EmailClient emailClient;
    private final Email email;


    public DropBoxUploader(ConfigService cfg, EmailClient emailClient, Email email) {
        this.cfg = cfg;
        this.emailClient = emailClient;
        this.email = email;
    }

    @Override
    public void upload(String path, String name) {
        String token = cfg.get(DROPBOX_TOKEN);
        DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/java-tutorial").build();
        DbxClientV2 client = new DbxClientV2(config, token);

        FullAccount account = null;
        try {
            account = client.users().getCurrentAccount();
        } catch (DbxException e) {
            e.printStackTrace();
        }
        System.out.println(account.getName().getDisplayName());
        try (InputStream in = new FileInputStream(path)) {
            FileMetadata metadata = client.files().uploadBuilder("/" + name)
                    .uploadAndFinish(in);
            //TODO move to external class EmailService
            //TODO wyslac maila
            //TODO jak to wszystko polaczyc w jedno
//            new Email(configService.get(EMAIL_ADDRESS),configService.get(EMAIL_CONTENT),configService.get(EMAIL_SUBJECT))
        } catch (IOException | DbxException e) {
            throw new UploadExcetpion("can not upload file" + path, e);
        }
    }
}
