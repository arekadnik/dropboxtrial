package com.ArkadiuszStadnik.dropbox.mail;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface EmailClient {
void send(Email email);
}

