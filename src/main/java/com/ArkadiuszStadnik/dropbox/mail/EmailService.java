package com.ArkadiuszStadnik.dropbox.mail;

import com.ArkadiuszStadnik.dropbox.listener.config.ConfigService;

import static com.ArkadiuszStadnik.dropbox.listener.config.Keys.EMAIL_ADDRESS;
import static com.ArkadiuszStadnik.dropbox.listener.config.Keys.EMAIL_CONTENT;
import static com.ArkadiuszStadnik.dropbox.listener.config.Keys.EMAIL_SUBJECT;

public class EmailService {


    ConfigService configService;
    //TODo wydaje mi sie ze powinno przyjmowac mailrpovider
    EmailClient emailClient;
    public EmailService(EmailClient emailClient) {
        this.emailClient = emailClient;
    }
    //TODO czy to powinno tak wygladac?czy ta klasa na pewno jest nam potrzebna ?

    public Email createMail() {
        return new Email(configService.get(EMAIL_ADDRESS), configService.get(EMAIL_CONTENT), configService.get(EMAIL_SUBJECT));
    }
}
