package com.ArkadiuszStadnik.dropbox.mail;

import com.ArkadiuszStadnik.dropbox.listener.config.ConfigService;
import com.ArkadiuszStadnik.dropbox.mail.client.MailJetProvider;
import com.ArkadiuszStadnik.dropbox.mail.client.SendGridProvider;

import static com.ArkadiuszStadnik.dropbox.listener.config.Keys.MAIL_CLIENT;

public class MailProvider {

    public static final String MAIL_JET = "MailJet";
    public static final String SEND_GRID = "SendGrid";

    public  EmailClient get(ConfigService configService) {
        String client = configService.get(MAIL_CLIENT);
        if (client.equals(MAIL_JET)) {
            return new MailJetProvider(configService);
        } else if (client.equals(SEND_GRID)) {
            return new SendGridProvider(configService);
        } else {
            throw new RuntimeException("No such client avaiilable");
        }

    }
}
